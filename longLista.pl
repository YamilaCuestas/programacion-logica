longLista([],0).

longLista([_|Y],N) :-
    longLista(Y, N1),
    N is N1 + 1.