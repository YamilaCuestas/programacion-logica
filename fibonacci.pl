fibonacci(0,0) :-
    X > 1,
    X1 is X-1,
    fibonacci(X1,Y1),
    X2 is X1-1,
    fibonacci(X2,Y2),
    Y is Y1+ Y2.

fibonacci(0,0).
fibonacci(1,1).
