% Trabajajo Práctico N°3
% Secuencias en PROLOG
% 3. Usando la base de datos generada en el problema de la familia de Luis, 
% y con lo que se conoce hasta ahora, escribir consultas para obtener 
% la siguiente información:

es_progenitor_de(ana,isabel).
es_progenitor_de(ana,carlos).
es_progenitor_de(guillermo,isabel).
es_progenitor_de(guillermo,carlos).
es_progenitor_de(carlos,roberto).
es_progenitor_de(carlos,amalia).
es_progenitor_de(carlos,luis).
es_progenitor_de(susana,roberto).
es_progenitor_de(susana,amalia).
es_progenitor_de(susana,luis).
es_progenitor_de(mercedes,susana).
es_progenitor_de(mercedes,angelica).
es_progenitor_de(ernesto,susana).
es_progenitor_de(ernesto,angelica).
es_progenitor_de(luis,federico).
es_progenitor_de(luis,carla).
es_progenitor_de(laura,federico).
es_progenitor_de(laura,carla).

es_varon(carlos).
es_varon(roberto).
es_varon(luis).
es_varon(guillermo).
es_varon(ernesto).
es_varon(federico).

es_mujer(susana).
es_mujer(amalia).
es_mujer(isabel).
es_mujer(ana).
es_mujer(mercedes).
es_mujer(angelica).
es_mujer(laura).
es_mujer(carla).

%Preguntas simples:

% a. ¿Es Carlos progenitor de Guillermo?
%?- es_progenitor_de(carlos,guillermo).
%false.

% b. ¿Es Jorge progenitor de Gonzalo?
%?- es_progenitor_de(jorge,gonzalo).
%false.

% c. ¿Es Carlos progenitor de Amalia?
%?- es_progenitor_de(carlos,amalia).
%true .


% d. Usando el predicado "padre" preguntar: ¿Es Carlos padre de Roberto?
 padre(X,Y):- es_progenitor_de(X,Y).
%?- padre(ana,isabel)
%true.

% e. ¿Gonzalo es mujer?
%?- es_mujer(gonzalo).
%false.

% f. ¿Alberto es varón?
%?- es_varon(alberto).
%false.


% Preguntas compuestas(Conjunciones)

% g. ¿Es cierto que Luis es progenitor de Carla, y que Carla es mujer?
% ?- es_progenitor_de(luis,carla),es_mujer(carla).
% true .

% h. ¿Es cierto que Ernesto es progenitor de Carlos, y que éste es progenitor
% de Luis, y a su vez Luis es progenitor de Carla
% ?- es_progenitor_de(ernesto,carlos),es_progenitor_de(carlos,luis),es_progenitor_de(luis,carla).
% false.

 