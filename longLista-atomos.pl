longLista([],0).

longLista([Ca|Co],N) :-
    longLista(Co, N1),
    longLista(Ca,N2),
    N is N1 +N2.

longLista(_,1).