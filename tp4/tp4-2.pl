% Universidad Nacional de Luján
% Programación Funcional y Lógica
% Trabajo Práctico nro. 4: Decisiones en Prolog
% 2. Definir un predicado ternario "mayor_o_igual" que relaciona dos
% números con el mayor de ambos, o con uno de ellos si son iguales.

mayor_o_igual(X,X,X).

mayor_o_igual(X,Y,Z):- X > Y , Z is X.

mayor_o_igual(X,Y,Z):- Y > X , Z is Y.