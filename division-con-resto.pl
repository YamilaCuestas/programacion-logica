division(Dividendo, Divisor, 0, Resto) :- 
    Divisor > Dividendo,
    Resto is Dividendo.

division(Dividendo, Divisor, Cociente, Resto) :-
    CAux is Dividendo - Divisor,
    division(CAux , Divisor, CAux2, Resto),
    Cociente is CAux2 + 1.