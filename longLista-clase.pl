longLista([],0).

longLista([Ca|Co],N) :-
    longLista(Co, N1),
    N is N1 + 1.