% Trabajajo Práctico N°3
% Secuencias en PROLOG
% 5. Definir un predicado que relacione una temperatura
%  expresada en grados Celsius con la misma temperatura 
%  expresada en grados Farenheit. Recordamos que F = (9/5) * C + 32

caf(C,F) :- F is (9/5) * C + 32.