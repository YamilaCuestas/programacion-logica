% Trabajajo Práctico N°3
% Secuencias en PROLOG
% 8. Definir un predicado que vincule las 
% notas de cuatro parciales con la nota promedio.

promedio(N1,N2,N3,N4, Promedio) :- Promedio is (N1+N2+N3+N4)/4.