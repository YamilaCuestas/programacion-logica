/* Clase de progrmación IV - 01 Junio 2018 */
duracion(actividad-a, 4).
duracion(actividad-b, 13).
duracion(actividad-c, 4).
duracion(actividad-d, 15).
duracion(actividad-e, 12).
duracion(actividad-f, 4).
duracion(actividad-g, 2).
duracion(actividad-h, 2).
duracion(actividad-i, 2).
precede(actividad-a, actividad-b).
precede(actividad-c, actividad-d).
precede(actividad-a, actividad-f).
precede(actividad-e, actividad-g).
precede(actividad-f, actividad-g).
precede(actividad-b, actividad-h).
precede(actividad-d, actividad-h).
precede(actividad-g, actividad-i).
precede(actividad-h, actividad-i).

/* es actividad todo aquello que tenga una duracion. */

actividad(X) :- duracion(X, Y).

/* es actividad inicial aquella que no tiene ninguna que la */ /*
 preceda. */

actividad-inicial(X) :- 
     actividad(X),
     not precede(Y, X).

/* es actividad final aquella que no precede a ninguna. */

actividad-final(A) :- 
     actividad(A),
     not precede(A, Y).


/* las actividades iniciales comienzan en tiempo cero. */

comienzo-temprano(X, 0) :-
     actividad-inicial(X).

/* el comienzo temprano es la mayor de las finalizaciones */
/* tempranas de las tareas que la preceden. */

comienzo-temprano(A, T) :-
   bagof(FT, final-temp-precedentes(FT, A), L),
   maximo(T, L).


final-temp-precedentes(FT, A) :-
   precede(Ap, A),
   finalizacion-temprana(Ap, FT).

/* la finalizacion temprana es el comienzo temprano mas la */
/*  duracion. */

finalizacion-temprana(A, FT) :-
   comienzo-temprano(A, CT),
   duracion(A, D),
   FT is CT + D. 


/* la duracion del proyecto es la mayor de las finaliz. */ 
/* tempranas de las actividades finales. */

duracion-de-proyecto(T) :-
   bagof(FT, finaliz-temp-finales(FT), L),
   maximo(T, L).

finaliz-temp-finales(FT) :-
   actividad-final(A),
   finalizacion-temprana(A, FT).

/* una actividad es critica si su comienzo temprano es */ 
/* igual a su comienzo tardio. */

critica(X) :-
   actividad(X),
   comienzo-temprano(X, Y),
   comienzo-tardio(X, Y).

/* la finalizazion tardia de una actividad final es la */
/* duración  del proyecto. */

finalizacion-tardia(X, Y) :-
   actividad-final(X),
   duracion-de-proyecto(Y).

/* la finalización tardía es el menor de los comienzos */
/* tardíos  de las actividades siguientes. */

finalizacion-tardia(X, Y) :-
   bagof(X1, comienzo-tardio-sig(X, X1), Z),
   minimo(Y, Z).

comienzo-tardio-sig(X, X1) :-
   precede(X, Y),
   comienzo-tardio(Y, X1).


/* el comienzo tardío es la finalización tardía menos la */
/* duración de la actividad. */

comienzo-tardio(X, Y) :-
   finalizacion-tardia(X, Z),
   duracion(X, X1),
   Y is Z - X1.

/* un camino critico parcial es una actividad final si esa */
/* actividad es critica. */

camino-critico-parcial([X]) :-
   actividad-final(X),
   critica(X).

/* una lista de actividades es un camino critico parcial */ 
/* si su  cabeza es una actividad critica y su cola es un */
/* camino critico parcial. */

camino-critico-parcial([X, Y|Z]) :-
   critica(X),
   precede(X, Y),
   camino-critico-parcial([Y|Z]).

/* una lista es un camino critico si es un camino critico */
/* parcial y su cabeza es una actividad inicial. */

camino-critico([X|Y]) :-
   actividad-inicial(X),
   camino-critico-parcial([X|Y]).

/* definiciones de maximos y minimos. */

maximo(X, [X]).
maximo(X, [X|Y]) :-
   maximo(Z, Y),
   Z =< X.
maximo(X, [Y|Z]) :-
   maximo(X, Z),
   Y =< X.

minimo(X, [X]).
minimo(X, [X|Y]) :-
   minimo(Z, Y),
   X =< Z.
minimo(X, [Y|Z]) :-
   minimo(X, Z),
   X =< Y.
