division(Dividendo, Divisor, 0, Dividendo) :- 
    Divisor > Dividendo.

division(Dividendo, Divisor, Cociente, Resto) :-
    CAux is Dividendo - Divisor,
    division(CAux , Divisor, CAux2, Resto),
    Cociente is CAux2 + 1.