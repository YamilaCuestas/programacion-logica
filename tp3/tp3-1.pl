% Trabajajo Práctico N°3
% Secuencias en PROLOG
% 1. Escribir las siguientes oraciones en Prolog

% a. El oro el valioso
valioso(oro).

% b. Isabel es mujer
es_mujer(isabel).

% c. Juan es Rey,
es_rey(juan).

% d. Zeus es el progenitor de Hercules.

progenitor_de(zeus,hercules).

% e. Jose le presta dinero a Pedro.

presta_dinero(jose,pedro).