% Universidad Nacional de Luján
% Programación Funcional y Lógica
% Trabajo Práctico nro. 4: Decisiones en Prolog
% 1.* Se tiene una base con los siguientes hechos:
% madre_de(ana, luis).
% ...
% padre_de(juan, luis).
% ...
% Construir una regla para definir el predicado "progenitor_de"


padre_de(juan,luis).
padre_de(juan,lucas).
madre_de(ana,lucas).
madre_de(ana,luis).


progenitor_de(X,Y) :- madre_de(X,Y).

progenitor_de(X,Y) :- padre_de(X,Y).