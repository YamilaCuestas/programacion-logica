MCD(X,X,X).
MCD(X,Y,M) :- 
    X > Y,
    R is X- Y,
    MCD(Y,R,M).

MCD(X,Y,M):-
    R is Y -X,
    MCD(X, R, M).