% Trabajajo Práctico N°3
% Secuencias en PROLOG
% 12. Se tiene la siguiente base de datos con ciudades 
% vecinas y las rutas que las conectan:

conectadas(arrecifes, san_antonio_de_areco, ruta_8).
conectadas(san_antonio_de_areco, cardales, ruta_8).
conectadas(carmen_de_areco, san_andres_de_giles, ruta_7).
conectadas(san_andres_de_giles, lujan, ruta_7).
conectadas(cardales, lujan, ruta_6).
conectadas(san_antonio_de_areco, san_andres_de_giles, ruta_41).
conectadas(san_andres_de_giles, mercedes, ruta_41).
conectadas(arrecifes, carmen_de_areco, ruta_51).
conectadas(carmen_de_areco, chivilcoy, ruta_51).
conectadas(lujan, mercedes, ruta_5).
conectadas(mercedes, chivilcoy, ruta_5).
siguiente(X, Y, Z) :- conectadas(Y, X, Z).

% Escribir las siguientes consultas:

% a) ¿Están conectadas San Andrés de Giles y Mercedes?
% ?- conectadas(san_andres_de_giles,mercedes,X)
% X = ruta_41

% b) Chivilcoy está conectada con San Antonio de Areco?
% ?- conectadas(chivilcoy,san_antonio_de_areco,X)
% false

% c) ¿La ruta 51 conecta Carmen de Areco con San Andrés de Giles?
% ?- conectadas(carmen_de_areco,san_andres_de_giles,ruta_51)
% false

% d) ¿Con quién está conectada Luján?
conectada_con(X,Y) :- conectadas(X,Y,_).
conectada_con(X,Y) :- conectadas(Y,X,_).
% ?- conectada_con(lujan,X)
% X = mercedes
% X = san_andres_de_giles
% X = cardales
% 

% e) ¿Cuál es la que está conectada con San Andrés de Giles?
% ?- conectada_con(san_andres_de_giles,X)
% X = lujan
% X = mercedes
% X = carmen_de_areco
% X = san_antonio_de_areco

% f) ¿Cuáles son las que están conectadas con San Andrés de Giles?
% g) ¿Qué rutas llegan a Luján?
% conectadas(lujan,_,X);
% conectadas(_,lujan,X)
% 
% h) ¿Qué rutas salen de Luján