% Trabajajo Práctico N°3
% Secuencias en PROLOG
% 11. La siguiente es la descripción de la familia Adams:

padre_de(homero, pericles).
padre_de(homero, merlina).
padre_de(desconocido, lucas).
padre_de(desconocido, homero).
varón(homero).
varón(lucas).
varón(cosa).
varón(pericles).
mujer(merlina).
mujer(morticia).
mujer(la-abuela).
madre(la-abuela, homero).
esposos(morticia, homero).

% Usando los predicados anteriores, definir la relación "hermano_de".

hermano_de(X,Y) :- padre_de(Z,X), padre_de(Z,Y), varón(X), X \= Y.

% Escriba las reglas necesarias para la consulta: ? tío(Quien,merlina).

tío(X,Y) :- padre_de(Z,Y), hermano_de(X,Z).