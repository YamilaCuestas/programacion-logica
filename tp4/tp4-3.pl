% Universidad Nacional de Luján
% Programación Funcional y Lógica
% Trabajo Práctico nro. 4: Decisiones en Prolog
% 3. Dada una base como:

perro(colita).
humano(juan).
%...
gato(kati).

% Escribir una regla para escribir en Prolog: "los perros, los gatos y los
% humanos son mortales".

mortal(X) :- perro(X).
mortal(X) :- humano(X).
mortal(X):- gato(X).
