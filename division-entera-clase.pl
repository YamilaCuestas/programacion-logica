division(Dividendo, Divisor, 0) :- Divisor > Dividendo.

division(Dividendo, Divisor, Cociente) :-
    CAux is Dividendo - Divisor,
    division(CAux , Divisor, CAux2),
    Cociente is CAux2 + 1.