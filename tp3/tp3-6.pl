% Trabajajo Práctico N°3
% Secuencias en PROLOG
% 6. Definir un predicado que relacione una longitud expresada en 
% centímetros, con la misma longitud expresada en pulgadas, pies y 
% yardas. Recordamos que:
% 1 yarda = 3 pies
% 1 pie = 12 pulgadas
% 1 pulgada = 2.54 centímetros
% 

cm_a_pulgadas(Cm,Pulgadas):- Pulgadas is (Cm/2.54) .

cm_a_pie(Cm,Pie):- cm_a_pulgadas(Cm,Pulgadas),
    Pie is (Pulgadas/12).

cm_a_yarda(Cm,Yarda):- cm_a_pie(Cm,Pie),
    Yarda is Pie/3.

% Todo junto
convertir_cm(Cm, Pulgadas,Pies,Yardas):- Pulgadas is (Cm/2.54),
    Pies is Pulgadas/12,
    Yardas is Pies/3.