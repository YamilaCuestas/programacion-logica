% Trabajajo Práctico N°3
% Secuencias en PROLOG
% 4. . Usando la base de datos generada en el problema de la familia 
% de Luis, y con lo que se conoce hasta ahora, escribir consultas para 
% obtener la siguiente información:

es_progenitor_de(ana,isabel).
es_progenitor_de(ana,carlos).
es_progenitor_de(guillermo,isabel).
es_progenitor_de(guillermo,carlos).
es_progenitor_de(carlos,roberto).
es_progenitor_de(carlos,amalia).
es_progenitor_de(carlos,luis).
es_progenitor_de(susana,roberto).
es_progenitor_de(susana,amalia).
es_progenitor_de(susana,luis).
es_progenitor_de(mercedes,susana).
es_progenitor_de(mercedes,angelica).
es_progenitor_de(ernesto,susana).
es_progenitor_de(ernesto,angelica).
es_progenitor_de(luis,federico).
es_progenitor_de(luis,carla).
es_progenitor_de(laura,federico).
es_progenitor_de(laura,carla).

es_varon(carlos).
es_varon(roberto).
es_varon(luis).
es_varon(guillermo).
es_varon(ernesto).
es_varon(federico).

es_mujer(susana).
es_mujer(amalia).
es_mujer(isabel).
es_mujer(ana).
es_mujer(mercedes).
es_mujer(angelica).
es_mujer(laura).
es_mujer(carla).


% CONSULTAS
% a. ¿Federico es progenitor de alguien?
% ?- es_progenitor_de(federico,X).
% false.

% b. ¿Quiénes son los progenitores de Roberto?
% ?- es_progenitor_de(X,roberto).
% X = carlos ;
% X = susana.

% c. ¿Guillermo tuvo alguna hija?
% ?- es_progenitor_de(guillermo,X),es_mujer(X).
% X = isabel .

% d. ¿Ernesto tuvo algún hijo varón?
% ?- es_progenitor_de(ernesto,X),es_varon(X).
% false.

% e. ¿Roberto y Amalia tienen el mismo padre?
% ?- es_progenitor_de(X,roberto),es_progenitor_de(X,amalia),es_varon(X).
% X = carlos .

% f. ¿Ernesto tuvo alguna hija que a su vez tenga una hija?
% ?- es_progenitor_de(ernesto,X),es_mujer(X),es_progenitor_de(X,Y),es_mujer(Y).
% X = susana,
% Y = amalia ;
% false.

